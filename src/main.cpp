#include <gtkmm.h>
#include <iostream>
#include <curl/curl.h>
#include <nlohmann/json.hpp>
#include <algorithm>
#include <cctype>
#include <string>
#include <sstream>
#include <locale>

int calculate_income_start(int gross_pay) {
    if (gross_pay > 1 && gross_pay <= 2000) {
        return 1;
    } else if (gross_pay >= 2001 && gross_pay <= 20000) {
        return gross_pay - ((gross_pay - 1) % 100);
    } else if (gross_pay >= 20001 && gross_pay <= 80000) {
        return gross_pay - ((gross_pay - 1) % 200);
    } else {
        return 0;
    }
}

std::string prepare_encoded_url(CURL *curl, const std::string& base_url, int tabellnr, int income_start, int year) {
    std::string encoded_url = base_url;

    // Encoding the query parameters
    char *encoded_tabellnr = curl_easy_escape(curl, std::to_string(tabellnr).c_str(), 0);
    char *encoded_income_start = curl_easy_escape(curl, std::to_string(income_start).c_str(), 0);
    char *encoded_year = curl_easy_escape(curl, std::to_string(year).c_str(), 0);

    // Constructing the full URL
    encoded_url += "?tabellnr=" + std::string(encoded_tabellnr);
    encoded_url += "&inkomst%20fr.o.m.=" + std::string(encoded_income_start);
    encoded_url += "&%C3%A5r=" + std::string(encoded_year);

    // Freeing the encoded strings
    curl_free(encoded_tabellnr);
    curl_free(encoded_income_start);
    curl_free(encoded_year);

    return encoded_url;
}

size_t WriteCallback(void *contents, size_t size, size_t nmemb, std::string *userp) {
    userp->append((char*)contents, size * nmemb);
    return size * nmemb;
}

int fetch_tax_data(int tabellnr, int gross_pay) {
    std::string url = "https://skatteverket.entryscape.net/rowstore/dataset/88320397-5c32-4c16-ae79-d36d95b17b95";
    int income_start = calculate_income_start(gross_pay);

    CURL *curl;
    CURLcode res;
    std::string readBuffer;

    curl = curl_easy_init();
    if (curl) {
        std::string full_url = prepare_encoded_url(curl, url, tabellnr, income_start, 2024);
        curl_easy_setopt(curl, CURLOPT_URL, full_url.c_str());
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, WriteCallback);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &readBuffer);

        res = curl_easy_perform(curl);
        if (res != CURLE_OK) {
            std::cerr << "curl_easy_perform() failed: " << curl_easy_strerror(res) << std::endl;
            curl_easy_cleanup(curl);
            return -1; // Indicate failure
        }

        // Parse JSON and extract data
        try {
            auto json = nlohmann::json::parse(readBuffer);
            std::string result = json["results"][0]["kolumn 1"].get<std::string>();
            curl_easy_cleanup(curl);
            return std::stoi(result);
        } catch (nlohmann::json::parse_error& e) {
            std::cerr << "JSON parsing error: " << e.what() << std::endl;
            curl_easy_cleanup(curl);
            return -1; // Indicate failure
        }
    }
    return -1; // Indicate failure if curl is not initialized
}

class MyWindow : public Gtk::Window {
public:
    MyWindow(BaseObjectType* cobject, const Glib::RefPtr<Gtk::Builder>& builder) : Gtk::Window(cobject), m_builder(builder) {
        button = nullptr;
        entry = nullptr;
        label = nullptr;
        combobox = nullptr;
        valid_entry = true;

        builder->get_widget("calculate_button", button);
        builder->get_widget("salary_entry", entry);
        builder->get_widget("net_salary", label);
        builder->get_widget("tax_bracket", combobox);

        if(button) {
            button->signal_clicked().connect(sigc::mem_fun(*this, &MyWindow::on_button_clicked));
        }

        if(entry) {
            entry->signal_changed().connect(sigc::mem_fun(*this, &MyWindow::on_entry_changed));
        }
    }

    static MyWindow* create() {
        auto builder = Gtk::Builder::create_from_file("my_interface.glade");
        
        MyWindow* window = nullptr;
        builder->get_widget_derived("main_window_id", window);
        return window;
    }

protected:
    void on_button_clicked() {
        if(!valid_entry) {
            return;
        }

        Glib::ustring gross_pay_text = entry->get_text();
        Glib::ustring tax_bracket_text = combobox->get_active_text();

        int gross_pay = std::stoi(gross_pay_text);
        int tax_bracket = std::stoi(tax_bracket_text);
        int tax_data = fetch_tax_data(tax_bracket, gross_pay);
        long long net_pay = static_cast<long long>(gross_pay - tax_data);

        std::stringstream ss;
        ss.imbue(std::locale("sv_SE.utf8"));

        // std::put_money uses std::numpunct::thousands_sep wich only returns a single code 
        // unit which is too short for the non-breaking space and results in invalud utf-8
        ss << "Nettolön: " << std::showbase << std::put_money(net_pay * 100);

        std::string formatted_money = ss.str();
        
        // Replace the invalid utf-8 sequence with the correct one for narrow non-breaking space
        size_t pos = 0;
        while((pos = formatted_money.find('\xe2', pos)) != std::string::npos) {
            formatted_money.replace(pos, 1, "\xe2\x80\xaf");
            pos += 3;
        }

        label->set_text(formatted_money);
    }

    void on_entry_changed() {
        const auto text = entry->get_text();

        valid_entry = validate_text(text);

        if(!valid_entry) {
            // Give error feedback to user
            entry->override_background_color(Gdk::RGBA("red"), Gtk::STATE_FLAG_NORMAL);
        } else {
            entry->unset_background_color(Gtk::STATE_FLAG_NORMAL);
        }
    }

    bool validate_text(const Glib::ustring& text) {
        if(text.empty()) {
            return false;
        }

        if(!std::all_of(text.begin(), text.end(), ::isdigit)) {
            return false;
        }

        int number = std::stoi(text.raw());
        if(number < 1) {
            return false;
        }

        return true;
    }

    Glib::RefPtr<Gtk::Builder> m_builder;
    Gtk::Button* button;
    Gtk::Entry* entry;
    Gtk::Label* label;
    Gtk::ComboBoxText* combobox;
    bool valid_entry;
};


int main(int argc, char** argv) {
    std::locale::global(std::locale("sv_SE.utf8"));
    auto app = Gtk::Application::create(argc, argv, "org.gtkmm.example");

    auto window = MyWindow::create();
    return app->run(*window);
}
