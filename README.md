# Räknat ut skatten



## Getting started

Install dependencies if needed

```
$ sudo apt update
$ sudo apt install cmake g++ libgtkmm-3.0-dev libcurl4-openssl-dev nlohmann-json3-dev
```

Compile project by creating and going to the build directory, then run the appropriate CMake commands from there.

```
mkdir -p build
cd build
cmake ..
make
```
